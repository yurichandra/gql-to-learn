import cookie from 'js-cookie'

export function set (key, data) {
  return cookie.set(key, data)
}

export function get (key) {
  return cookie.get(key)
}

export function clear (key) {
  return cookie.remove(key)
}

import Vue from 'vue'
import VueRouter from 'vue-router'

import Login from '@/views/Login'
import Dashboard from '@/views/Dashboard'

import auth from '@/middlewares/auth'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'dashboard',
    component: Dashboard,
    beforeEnter: auth
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

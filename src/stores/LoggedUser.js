import { apollo } from '@/plugins/apollo'
import { WHOAMI } from '@/graphql/query/whoami'
import { SESSION_NAME } from '@/constants/session'
import { set, clear } from '@/helpers/session'

const state = {
  isAuthenticated: false,
  whoamiError: null,
  loggedUser: {
    id: 0,
    name: '',
    email: ''
  }
}

const getters = {
  isAuthenticated: state => state.isAuthenticated,
  loggedUser: state => state.loggedUser
}

const actions = {
  storeToken ({ commit }, token) {
    set(SESSION_NAME, token)

    commit('authenticateSuccess')
  },

  async whoami ({ commit }) {
    try {
      const data = await apollo.query({ query: WHOAMI })

      const user = {
        id: data.data.whoami.id,
        name: data.data.whoami.personal.fullName,
        email: data.data.whoami.personal.email
      }

      commit('setLoggedUser', user)
      commit('whoamiSuccess')
    } catch (err) {
      commit('whoamiFail')
    }
  },

  logout ({ commit }) {
    const data = {
      id: 0,
      name: '',
      email: ''
    }

    clear(SESSION_NAME)

    commit('setLoggedUser', data)
    commit('logout')
  }
}

const mutations = {
  setLoggedUser (state, data) {
    state.loggedUser = {
      ...data
    }
  },

  authenticateSuccess (state) {
    state.isAuthenticated = true
  },

  whoamiSuccess (state) {
    state.isAuthenticated = true
  },

  whoamiFail (state, err) {
    state.isAuthenticated = false
    state.whoamiError = err
  },

  logout (state) {
    state.isAuthenticated = false
    apollo.cache.reset()
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}

import { get } from '@/helpers/session'
import { SESSION_NAME } from '@/constants/session'
import store from '@/store'

export default async function auth (to, from, next) {
  const session = get(SESSION_NAME)

  if (!session) {
    next('/login')

    return
  }

  await store.dispatch('whoami')
  next()
}

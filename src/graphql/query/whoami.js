import gql from 'graphql-tag'

export const WHOAMI = gql`
  query {
    whoami {
      id
      username
      personal {
        fullName
        gender
        email
        jobUnit {
          id
          name
        }
      }
      role {
        id
        name
      }
    }
  }
`

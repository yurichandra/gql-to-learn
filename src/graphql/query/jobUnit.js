import gql from 'graphql-tag'

const JOB_UNIT_QUERY = gql`
  query {
    jobUnits {
      id
      name
      jobPositions {
        id
        name
      }
    }
  }
`

export default JOB_UNIT_QUERY

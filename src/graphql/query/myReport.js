import gql from 'graphql-tag'

const MY_REPORT_QUERY = gql`
  query {
    myReports {
      id
      jobName
      lat
      lng
      duration
      description
      contractNumber
      images {
        fileName
        mimeType
        url
      }
      user {
        id
        username
      }
    }
  }
`

export default MY_REPORT_QUERY

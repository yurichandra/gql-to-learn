import gql from 'graphql-tag'

const ROLE_QUERY = gql`
  query {
    roles {
      id
      name
    }
  }
`

export default ROLE_QUERY

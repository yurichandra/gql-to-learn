import gql from 'graphql-tag'

export const LOGIN_MUTATION = gql`
  mutation ($username: String, $password: String) {
    authenticate(username: $username, password: $password)
  }
`

import Vue from 'vue'
import VueApollo from 'vue-apollo'

import { get } from '@/helpers/session'
import { SESSION_NAME } from '@/constants/session'

import ApolloClient from 'apollo-boost'

Vue.use(VueApollo)

export const apollo = new ApolloClient({
  uri: process.env.VUE_APP_GQL_BASE,
  request: (operation) => {
    operation.setContext({
      headers: {
        Authorization: `Bearer ${get(SESSION_NAME)}`
      }
    })
  }
})

const apolloProvider = new VueApollo({
  defaultClient: apollo
})

export default apolloProvider
